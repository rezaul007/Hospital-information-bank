<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class doctorsinfo extends CI_Controller {
public function doctors(){
	    $data = array();
	    $id=$this->uri->segment(3);
		$data['pg_title'] = "Hospital information bank";
		$data['model'] = $this->mm->doctors_table($id);		
		$data['content'] = $this->load->view("doctorsinf", $data, true);
		$this->load->view('master', $data);
	}
}
	
?>