
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addspecialist extends CI_Controller {
public function index()
{ 
		$data = array();
		$data['pg_title'] = "Hospital information bank";
		$data['model'] =$this->mm->view_data("hospitals");
		$data['content'] = $this->load->view("add_specialist_form", $data, true);
		$this->load->view('master', $data);
	}
	public function insert()
	{
		$data = array(
						"sp_name" =>$this->input->post('hspnm'),
						"hosp_id" =>$this->input->post('areaid')
					);
		if($this->mm->insert('specialist',$data))
		{
			$message['msg'] = "Saved Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Addspecialist/view_info");
	}
	public function view_info()
	{
		$data = array();
		$data['model'] = $this->mm->view_data_multiple_table('specialist','hospitals','specialist.sp_id,specialist.sp_name,hospitals.hosp_name ','specialist.hosp_id = hospitals.hosp_id');   //view_data_two_table($table1,$table2,$select,$relation)
		$data['content'] = $this->load->view('specialist_name_view',$data,true);
		$this->load->view('master',$data);
	}
	public function edit_data()
	{
		$id = array("sp_id" => $this->uri->segment(3));
		
		$data = array();
		$data['all_area'] = $this->mm->view_data('hospitals');
		$data['model'] = $this->mm->edit_data('specialist',$id);
		$data['content'] = $this->load->view('specialist_form_edit',$data,true);
		$this->load->view('master',$data);
		
	}
	public function update()
	{
		$id = array("sp_id" => $this->input->post('id'));
		$data = array(
						"sp_name" => $this->input->post('nm'),
						"hosp_id" =>$this->input->post('cid')
					);
	
		if($this->mm->update_data('specialist',$data,$id))
		{
			$message['msg'] = "Update Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Addhospital/view_info" , "refresh");
	}
	public function delete_data()
	{
		$id = array("sp_id" => $this->uri->segment(3));
		
		if($this->mm->delete_data('specialist',$id))
		{
			$message['msg'] = "Deleted Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		//$this->session->set_userdata($message);
		redirect(base_url()."Addspecialist/view_info" , "refresh");	
	}
	

}	
?>