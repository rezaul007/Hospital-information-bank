
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addhospital extends CI_Controller {
public function index()
{ 
		$data = array();
		$data['pg_title'] = "Hospital information bank";
		$data['model'] =$this->mm->dropdawn_areaname();
		$data['content'] = $this->load->view("add_hospital_form", $data, true);
		$this->load->view('master', $data);
	}
	public function insert()
	{
            
		$data = array(
						"hosp_name" =>$this->input->post('hspnm'),
                                                "hosp_add" => $this->input->post('hosp_add'),
                                                "hosp_con" => $this->input->post('hosp_con'),
                                                "Email" => $this->input->post('email'),
                                                "Web" => $this->input->post('web'),
						"area_id" =>$this->input->post('areaid')
					);
		if($this->mm->insert('hospitals',$data))
		{
			$message['msg'] = "Saved Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Addhospital/view_info");
	}
	public function view_info()
	{
		$data = array();
		$data['model'] = $this->mm->view_data_multiple_table('hospitals','area','hospitals.hosp_id,hospitals.hosp_name,area.area_name ','hospitals.area_id = area.area_id');   //view_data_two_table($table1,$table2,$select,$relation)
		$data['content'] = $this->load->view('hospital_name_view',$data,true);
		$this->load->view('master',$data);
	}
	public function edit_data()
	{
		$id = array("hosp_id" => $this->uri->segment(3));
		
		$data = array();
		$data['all_area'] = $this->mm->view_data('area');
		$data['model'] = $this->mm->edit_data('hospitals',$id);
		$data['content'] = $this->load->view('hospital_form_edit',$data,true);
		$this->load->view('master',$data);
		
	}
	public function update()
	{
		$id = array("hosp_id" => $this->input->post('id'));
		$data = array(
						"hosp_name" => $this->input->post('nm'),
                                                "hosp_add" => $this->input->post('hosp_add'),
                                                "hosp_con" => $this->input->post('hosp_con'),
                                                "Email" => $this->input->post('email'),
                                                "Web" => $this->input->post('web'),
						"area_id" =>$this->input->post('cid')
					);
	
		if($this->mm->update_data('hospitals',$data,$id))
		{
			$message['msg'] = "Update Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Addhospital/view_info" , "refresh");
	}
	public function delete_data()
	{
		$id = array("hosp_id" => $this->uri->segment(3));
		
		if($this->mm->delete_data('hospitals',$id))
		{
			$message['msg'] = "Deleted Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		//$this->session->set_userdata($message);
		redirect(base_url()."Addhospital/view_info" , "refresh");	
	}
	

}	
?>