 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	public function index()
	{
		$data = array();
		$data['menu'] = "hospital";
		$data['pg_title'] = "Hospital information bank";
		$data['model'] = $this->mm->dropdawn_areaname();		
		$data['content'] = $this->load->view("home", $data, true);
		$this->load->view('master', $data);
	}
	
}
?>
