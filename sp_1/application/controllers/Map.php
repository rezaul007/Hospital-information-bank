<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {
    public function index()
    {
        $this->load->library('googlemaps');
        $from=$this->input->post("from");
        $to=$this->input->post("to");
        $config['center'] = 'Dhaka, Bangladesh';
        $config['zoom'] = 'auto';
        $config['directions'] = TRUE;
        $config['directionsStart'] = $from;
        $config['directionsEnd'] = $to;
        $config['directionsDivID'] = 'directionsDiv';
        $this->googlemaps->initialize($config);
        $data['map'] = $this->googlemaps->create_map();
        $this->load->view('view_map', $data);
    }
}
?>