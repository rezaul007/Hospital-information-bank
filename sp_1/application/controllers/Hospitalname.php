<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hospitalname extends CI_Controller {
	
public function index()
{ 
        
		$data = array();
		$id=$this->input->post('id');
		$this->session->set_userdata($id);
		$data['pg_title'] = "Hospital information bank";
		$data['model'] =$this->mm->view_data_two_table($this->input->post('name'));
                $data['model2']=$this->mm->dropdawn_areaname();
		$data['content'] = $this->load->view("hospital_name", $data, true);
		$this->load->view('master', $data);

}
public function hospSearch(){
                $data = array();
		$data['pg_title'] = "Hospital information bank";
		$data['model'] =$this->mm->hospital_search($this->input->POST('hospnm'));
                if($data['model'] != NULL){
		$data['content'] = $this->load->view("hospital_name", $data, true);
                }
                else{
                    $data['content'] = $this->load->view("sorry", $data, true);
                }
		$this->load->view('master', $data);

}

}
?>