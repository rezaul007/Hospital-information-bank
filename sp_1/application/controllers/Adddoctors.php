
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adddoctors extends CI_Controller {
public function index() 
{ 
		$data = array();
		$data['pg_title'] = "Hospital information bank";
                $data['all_hospital'] =$this->mm->view_data("hospitals");
		$data['all_specialist'] =$this->mm->view_data("specialist");
		$data['content'] = $this->load->view("doctor_from_entry", $data, true);
		$this->load->view('master', $data);
	}
	public function insert()
	{
		$data = array(
						"doc_name" => $this->input->post('nm'),
						"doc_deg" =>$this->input->post('deg'),
                                                "doc_time" =>$this->input->post('time'),
                                                "sp_id" =>$this->input->post('sp_id')
					);
		if($this->mm->insert('doctors',$data))
		{
			$message['msg'] = "Saved Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Adddoctors/view_info");
	}
	public function view_info()
	{
		$data = array();
		$data['model'] = $this->mm->doctors_info();  
               
		$data['content'] = $this->load->view('doctors_name_view',$data,true);
		$this->load->view('master',$data);
	}
	public function edit_data()
	{
		$id = array("doc_id" => $this->uri->segment(3));
		
		$data = array();
		$data['all_specialist'] = $this->mm->view_data('specialist');
                $data['all_hospital'] = $this->mm->view_data('hospitals');
		$data['model'] = $this->mm->edit_data('doctors',$id);
		$data['content'] = $this->load->view('doctors_from_edit',$data,true);
		$this->load->view('master',$data);
		
	}
	public function update()
	{
//           var_dump($_POST);
//            die();
		$id = array("doc_id" => $this->input->post('id'));
		$data = array(
                                                "doc_time" =>$this->input->post('time'),
						"doc_name" => $this->input->post('nm'),
						"doc_deg" =>$this->input->post('deg'),
                                                
                                                "sp_id" =>$this->input->post('sp_id')
					);
	
		if($this->mm->update_data('doctors',$data,$id))
		{
			$message['msg'] = "Update Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		$this->session->set_userdata($message);
		redirect(base_url()."Adddoctors/view_info" , "refresh");
	}
	public function delete_data()
	{
		$id = array("doc_id" => $this->uri->segment(3));
		
		if($this->mm->delete_data('doctors',$id))
		{
			$message['msg'] = "Deleted Successfully";
		}
		else
		{
			$message['emsg'] = "Database too busy";
		}
		//$this->session->set_userdata($message);
		redirect(base_url()."Adddoctors/view_info" , "refresh");	
	}
	

}	
?>