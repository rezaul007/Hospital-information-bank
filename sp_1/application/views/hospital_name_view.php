<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<style type="text/css">
table{border-collapse:collapse;}
.tableHeader{font-size:24px;color:#000;padding:5px 0px;}
.tdStyle{text-align:center;}
.btnStyle{padding:0px 20px;}
#message1{color:green; font-size:24px;}
#message2{color:red; font-size:24px;}
</style>
<body>
	<?php
	
	$m = $this->session->userdata('msg');
	if($m != null)
	{
		echo "<span id='message1'>{$m}</span>";
		$this->session->unset_userdata('msg');	
	}
	$em = $this->session->userdata('emsg');
	if($em != null)
	{
		echo "<span id='message2'>{$em}</span>";
		$this->session->unset_userdata('emsg');	
	} 
?>	
<br />
<a href="<?php echo base_url(); ?>Addhospital" style="padding-left: 230px"><button>Add hospital</button></a><br /><br />
 <table border="1" style="margin: auto">
            <tr><th colspan="5" class="tableHeader">Hospital Name </th></tr>
           	<tr>
               <th>Serial No.</th>
               <th>Hospital name</th>
               <th>Area</th>
               <th colspan="2">Action</th>
            </tr>
  <?php 
  		$serial = 0;
		foreach($model as $d)
		{							
			$serial++;						
		echo "<tr>";
		echo "<td class='tdStyle'>{$serial}</td>";
		echo "<td>{$d->hosp_name}</td>";
		echo "<td>{$d->area_name}</td>";
		echo "<td>";
			echo "<a href='".base_url()."Addhospital/edit_data/{$d->hosp_id}'><button>Edit</button></a>";
		echo "</td>";
		echo "<td>";
			echo "<a href='".base_url()."Addhospital/delete_data/{$d->hosp_id}'><button>Delete</button></a>";
		echo "</td>";
		echo "</tr>";
		}
  ?>
    	
        </table>
                                  
</body>
</html>