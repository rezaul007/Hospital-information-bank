<!DOCTYPE html>
<!--Website template by freewebsitetemplates.com-->
<html>
<head>
	<meta charset="UTF-8">
	<title>hospital information bank</title>
        
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="js/bjqs-1.3.min.js"></script>
    
     <script type="text/ecmascript">
	/*jQuery(document).ready(function($) {
	$('#banner-fade').bjqs({
		'height' : 360,
		'width' : 1100,
		'responsive' : true,
		'animspeed' : 6000,
		'animtype' : 'fade',
		'showmarkers' : true,
		'automatic' : true
	});
});*/

	</script>
	
    
</head>
<body>
    
<div id="wraper">
    <div style="align-self: center">
	<div id="header">
		<div>
			<!--<div> -->
				<h1>HOSPITAL INFORMATION BANK</h1>
		<!--	</div> -->
          <div id="slider">
<!--          <div id="banner-fade">-->
        <!-- start  Slider -->
<!--        <ul class="bjqs">-->
            <li><img src="<?php echo base_url();?>images/hospital.jpg" title="Automatically generated caption" height="360px" width="1150px"></li>
<!--            <li><img src="<?php echo base_url();?>images/hospital-management-information-system.jpg" title="Automatically generated caption" height="360px" width="1150px"></li>-->
<!--          <li><img src="<?php //echo base_url();?>images/Rehabilitation-Hospital1.png" title="Automatically generated caption"></li>
          <li><img src="<?php //echo base_url();?>images/lsms.jpg" title="Automatically generated caption"></li>-->
          
<!--        </ul>-->
        <!-- end Slider -->

<!--      </div>		-->
         </div>
         <br /> <br />
        
            
   		</div>
            </div>
        </div>
            <?php
           
         $id = $this->session->userdata("id");  
          //var_dump($id);
         if($id != NULL){
         ?>
    <div  class="menubar">
            <ul>
         <li><a href="<?php echo base_url(); ?>Addhospital/view_info">Hospital management</a></li>    
         <li><a href="<?php echo base_url(); ?>Addspecialist/view_info">Specialist management</a> </li>   
         <li><a href="<?php echo base_url(); ?>Adddoctors/view_info">Doctor management</a>  </li>  
         <li><a href="<?php echo base_url(); ?>Authentication/logout">Logout</a> </li>   
         <br />
         
       </ul>
        </div>
<?php } ?>
	
    <div id="body">
	<?php if (isset($content)) echo $content;?>
    </div>
	<div id="footer">
		<div>
			<div>
				<a href="http://freewebsitetemplates.com/go/twitter/" target="_blank" id="twitter">Twitter</a>
				<a href="http://freewebsitetemplates.com/go/facebook/" target="_blank" id="facebook">Facebook</a>
				<a href="http://freewebsitetemplates.com/go/googleplus/" target="_blank" id="googleplus">Google&#43;</a>
			</div>
			<p>
				&copy; Copyright 2016. All rights reserved
			</p>
		</div>
	</div>
  </div>
    </div>
</body>
</html>