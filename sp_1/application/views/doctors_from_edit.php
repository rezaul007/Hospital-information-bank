<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<style type="text/css">
#message1{font-size:24px;color:green;}
#message2{font-size:24px;color:red;}

</style>
<script>
    function CatScat(data){
                //data.hospid.options.length = 0;
                var x = data.hospid.options[data.hospid.selectedIndex].value;
                if(x == 0){
                    data.sp_id.options[data.sp_id.options.length] = new Option("Select hospital First", 0);
                }
                <?php
                foreach ($all_hospital as $cat){                 
             ?>
                else if(x == <?php echo $cat->hosp_id ?>){
                    <?php
             foreach ($all_specialist as $scat){
                 if($scat->hosp_id == $cat->hosp_id) {
                    ?>
                    data.sp_id.options[data.sp_id.options.length] = new Option("<?php  echo $scat->sp_name;?>", <?php echo $scat->sp_id; ?>);
             <?php } } ?>
                }               
                <?php } ?>     
                
                
            }
</script>
<body>
	<?php
    	foreach($model as $d)
		{
	?>		
        
    <form action="<?php echo base_url();?>Adddoctors/update"  method="post" name="myform">
        	<input type="hidden" name="id" value="<?php echo $d->doc_id;?>" />
			<fieldset>
			<h2 class="sub-title">Edit Form</h2>
			<span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;doctor Name</span></span>
                        <input type="text" name="nm" required class="form-control input-lg" placeholder="doctor Name" value="<?php echo $d->doc_name;?>"><br />
			<span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;doctor degree</span></span>
			<input type="text" name="deg" required class="form-control input-lg" placeholder="doctor degree" value="<?php echo $d->doc_deg;?>"><br />
                            <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;doctor time</span></span>
			<input type="text" name="time" required class="form-control input-lg" placeholder="doctor time" value="<?php echo $d->doc_time;?>"><br />
            
			<span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;Hospital's name</span></span>
                    <select name="hospid" class="form-control input-lg" onchange="CatScat(document.myform)">                 
                        <option value="0">Select hospital</option>
                         <?php
                        foreach ($all_hospital as $d) {
                            echo "<option value='{$d->hosp_id}'>{$d->hosp_name}</option>";
                        }
                        ?>
                    </select>
                       <span class="input-group-addon"><span class="input-text-style">&nbsp;&nbsp;&nbsp;specialist unit</span></span>
                    <select name="sp_id" class="form-control input-lg">
                       
                    </select><br />
 
            	
			</fieldset>
			<input type="submit"  value="Update" class="btn btn-custom-2 btn-lg md-margin">
		</form>
        
     <?php }?>                             
</body>
</html>