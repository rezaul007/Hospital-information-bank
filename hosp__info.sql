-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2016 at 05:27 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hosp_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL auto_increment,
  `area_name` varchar(100) NOT NULL,
  PRIMARY KEY  (`area_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`area_id`, `area_name`) VALUES
(1, 'Gulshan'),
(2, 'Banani'),
(3, 'Dhanmondi'),
(4, 'Uttara'),
(5, 'Green Road'),
(6, 'Eskaton Road'),
(7, 'Shamoli'),
(8, 'Mirpur'),
(9, 'Muhammad Pur'),
(10, 'Pantha Path'),
(11, 'Mohakhali');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `doc_id` int(11) NOT NULL auto_increment,
  `doc_name` varchar(50) NOT NULL,
  `doc_deg` varchar(100) NOT NULL,
  `doc_time` varchar(40) NOT NULL,
  `sp_id` int(11) NOT NULL,
  PRIMARY KEY  (`doc_id`),
  KEY `sp_id` (`sp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`doc_id`, `doc_name`, `doc_deg`, `doc_time`, `sp_id`) VALUES
(1, 'Dr. Chowdhury Habibur Rahman', 'FRCS.FICS,General and Orthopaedic Surgeon,Ex. Head of the Dept. of Surgery', 'Consulting Hour: 9.30 am-11am, By Appoin', 1),
(2, 'Dr. S. F. Begum', 'BSc. MBBS,Ex. Registrar In Obstetrics & Gynaecology South London Hospital for Woman,Ex. S.H.O. in Pe', 'Consulting Hour: 11 am-2pm, By Appointme', 2),
(3, 'Dr. Afroza H. Ahmed', 'MBBS. MPH. FCPS', 'Consulting Hour: 6pm-8pm, By Appointment', 3),
(4, 'Dr. Md. Tawfique Hossain Chowdhury', 'BDS (Dhaka), MSc (London), DDPHRCS (England),Trained in ART & Aesthetic Dentistry (Netherlands),Comm', 'Consulting Hour: 5pm-9pm, By Appointment', 4),
(5, 'Dr. Md. Shamsul Alam', 'MBBS (Dhaka), MCPS (Surgery),MS(General Surgeon, Cardiothorasic Heart & Chest),Assistant Professor (', 'Consulting Hour: 7pm-8pm, By Appointment', 5),
(6, 'Dr. Sabina Parveen', 'MBBS, MCPS, BGO, FCPS, CCU, DMU', 'Consulting Hour10am-1pm, By Appointment ', 2),
(7, 'Prof. Abdullah A. Haroon', 'F.C.P.S (B.D) F.R.C.S (Glasgow),Former Professor & Head of Dept.,ENT & Head Neck Surgery', 'Consulting Hour: 12noon-1pm, By Appointm', 6),
(8, 'Dr. Towhiduzzaman', 'BSPT (Nitor), DU,PGT (BSMMU)', 'Consulting Hour: 11am-12.30 noon5pm-8pm,', 7),
(9, 'Dr. Manzoor Mahmood', 'MBBS (DMC), D. Card (DU), MD (Card),MRCP (UK), MSc (London) FACC (USA), FESC,Internal Medicine Speci', 'Consulting Hour: 5pm-7pm, By Appointment', 8),
(10, 'Dr. Humaira Alam', 'MBBS, FCPS,Infertility Specialist & Laparoscopic Surgeon', 'Consulting Hour: 5pm-6pm. By Appointment', 2),
(11, 'Dr. Major S.M. Sarwar (Retd.)', 'MBBS. (PAK) MCPS, DLO, FCPS,Advance Training UK,,Consultant ENT & Head Neck SPecialist & Surgeon', 'Consulting Hour: 3pm-6pm, By Appointment', 9),
(12, 'Dr. Murad Chowdhury', 'MBBS. MS (Urology),Assistant Professor (Urology)', 'Consulting Hour: 6.30pm-7.30pm,By Appoin', 10),
(13, 'Dr. S.M. Kamrul Hasan', 'MBBS, MPH, MD (Cardiology),Medicine & Cardiology Specialist', 'Consulting Hour:5.30pm-7.30pm,By Appoint', 8),
(14, 'Dr. Kh. Farida Begum', 'MBBS (DMC), MCPS (Gynae & Obst),Specialist in obstetrics & Gynaecologist,Ex. Consultant Holy Family ', 'Consulting Hour: 11am-1pm, By Appointmen', 2),
(15, 'Dr. Ashok Kumar Datta', 'MBBS, DLO, PGT, (Thailand),ENT Specialist & Head, Neck Surgeon', 'Consulting Hour: 3pm-5pm, By Appointment', 6),
(16, 'Dr. Rokeya Begum', 'FCPS, MCPS, DDV, MBBS,Assistant Professor Dept. of Dermatology & Venereology,Skin & Allergy Speciali', 'Consulting Hour: 5pm-6pm, By Appointment', 11),
(17, 'Dr. A.B.M Sarwar-E-Alam', 'MBBS, FCPS', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(18, 'Dr. Mirza Nazim Uddin', 'MBBS, MRCP (UK),Deputy Director,Consultant, Internal Medicie & Intensivist (ICU)', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(19, 'Dr.Jahangir Alam', 'MBBS (DMC), MRCP (UK),Consultant, Internal Medicine and Diabetes', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(20, 'Dr. Abu Reza Mohammad Nooruzzaman', 'MBBS, MRCP (UK),Consultant, Internal Medicine & Critical Care', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(21, 'Dr. Kazi Ali Hassan', 'MBBS, M.Phil (EM), MRCP (UK),Consultant, Endocrinology', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(22, 'Prof. Dr. Dilip Kumar Dhar', 'MBBS, FCPS (Medicine),Consultant, Internal Medicine', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(23, 'Dr. Zubayer Ahmed', 'MBBS, MD (Internal Medicine),Clinical Fellow (Rheumatology and Immunology- Singapore),Consultant, In', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(24, 'Dr. Raihan Rabbani', 'MBBS, FCPS, US Board Certified in Internal Medicine,Consultant, Internal Medicine', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12),
(25, 'Dr. Pratik Dewan', 'MBBS, DEM, MD (Internal Medicine), BIRDEM Academy ,Consultant, Endocrinology', 'Consulting Hour: 10am-1pm, & 4pm-8pm, By', 12);

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `hosp_id` int(11) NOT NULL auto_increment,
  `hosp_name` varchar(100) NOT NULL,
  `hosp_add` varchar(200) NOT NULL,
  `hosp_con` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Web` varchar(100) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`hosp_id`),
  UNIQUE KEY `hosp_add` (`hosp_add`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `hospitals`
--

INSERT INTO `hospitals` (`hosp_id`, `hosp_name`, `hosp_add`, `hosp_con`, `Email`, `Web`, `area_id`) VALUES
(33, 'Cure Medical Centre Ltd. & General Hospital', 'House No. 5, Road No. 16, Gulshan-1, Dhaka-1212', 'Phone: 9894776, 984054, Mobile: 01752151445', 'N/A', 'N/A', 1),
(34, 'SQUARE Hospitals Ltd.', '18/F,Bir Uttam Qazi Nuruzzaman Sarak,West Panthapath , Dhaka-1205', 'Tel:(880-2)8141522,8142431', 'info@squarehospital.com', 'www.squarehospital.com or www.facebook.com/squarehospital', 10),
(35, 'United Hospital Ltd.', 'Plot 15, Road 71,Gulshan 2, Dhaka-1212', 'Tel:+88028836000,8836444,01914001313, Emergency:01', 'N/A', 'www.uhlbd,com', 1),
(36, 'Labaid', 'House#13/A,Road#35,Gulshan 2,Dhaka-1212', 'Tel:8835981-4,9858943, Cell:01552463101, Hotline:0', 'N/A', 'Web:www.labaidgroup.com', 1),
(37, 'Labaid', 'House#1,Road#4,Dhanmondi,Dhaka 1205', 'Phone:58610793-8,9670210-3', 'Email:info@labaidgroup.com', 'Web:www.labaidgroup.com', 3),
(38, 'Prescription Point Diagnostic & Health Care', 'House:105,Road#12,Block#E,Banani,Dhaka-1213', 'Phone:9897222,9851752, Mbl:01713 333233,01713 3332', 'Email:info@ppointbd.com', 'N/A', 2),
(39, 'Bangladesh Eye Hospital Ltd.', '78,Satmosjid Road(West of Road 27) Dhanmondi, Dhaka-1205', 'Phone(+88)02-9102264,(+88)09666787878, Mbl:(+88)01', 'Email:bdhospital@yahoo.com', 'Web:www.bdeyehospital.com', 3),
(40, 'Harun Eye Hospital', 'House#12/A,R#5,Dhanmondi,Dhaka,Bangladesh', 'Mbl:01918185620,01819286430', 'N/A', 'N/A', 3),
(41, 'Uttara Central Hospital', 'House#01,Road#07,Sector#01,Uttara Model Town,Dhaka', 'Tel:8918778, Mbl:01711-182522', 'N/A', 'N/A', 4),
(42, 'Ibn Sina Diagnostic & Imaging Center', 'House#48,Road#9/A,Satmosjid Road,Dhanmondi,Dhaka-1209', 'Tel: 9126625-6,9128835-7', 'info@ibnsinatrust.com ', 'www.ibnsinatrust.com', 3),
(43, 'RIVIC Hospital& Diagnostic Complex Ltd.', 'House-19, Road-5, Sector-7, Uttara, Dhaka', 'Phone : 58953904,8952157 ', 'N/A', 'N/A', 4),
(44, 'Medinova', 'House No. 71 /A, Road No. 5/A, Dhanmondi R/A, Dhaka.', 'Phone : 02-58610661-5, 02-58610682-4 Hotline: 0179', 'info@medinovamedicalbd.com ', 'www.medinovamedicalbd.com', 3),
(45, 'Z.H. HAQUE SIKDER CARDIAC CARE & RESEARCH CENTRE', 'House # 05, Road # 104, Gulshan-2, Dhaka-1213 ', 'Tel # +88-02-8815363, 9887458, ', 'zhswmch@bangala.net', 'N/A', 1),
(46, 'SIBL Foundation Hospital & Diagnostic Centre', 'Fattah Plaza,70 Green Road, Panthapath,Dhaka-1205', 'Tel:+88029641297, Hotline:01991150900', 'Email:sibl.hospital@sibl-bd.com', 'N/A', 5),
(47, 'The Medical Centre Ltd.', 'House # 84, Road # 7/A, Satmosjid Road, Dhanmondi,Dhaka-1205', 'Appointment: 9118219, 9135381', 'N/A', 'N/A', 3),
(48, 'Prime Bank Eye Hospital', 'House#82,Road#8/A,Satmosjid Road,Dhanmondi,Dhaka-1209', 'Phone- 9125529, 9125530 Mobile- 01721068282', 'N/A', 'N/A', 3),
(49, 'Japan Bangladesh Friendship Medical Services Ltd.', '55, Satmasjid Road (Zigatola Bus Stand), Dhanmondi, Dhaka-1209', 'Tel: 9676161,9672277,9674535 9664028', 'jbfh24@yahoo.com', 'www.jbfh.org.bd', 3),
(50, 'RMC Hospital & Diagnostic Complex Ltd.', 'House#19,Road#5,Sector#7,Uttara Model Town,Dhaka-1230', 'Tel:8923904,8952157, Mbl:01817-049140', 'N/A', 'N/A', 4),
(51, 'Anwer Khan Moder Medical College Hospital', 'House#17,Road#8,Dhanmondi,Dhaka-1205', 'Tel:9670295,8613883,8616074,01199843758', 'N/A', 'N/A', 3),
(52, 'Instetute Of Laser Surgery & Hospital', '5,New Eskaton Road(Gausnagar),Dhaka-1000', 'Phone#9347520-1', 'N/A', 'N/A', 6),
(53, 'SPRC & Neurology Hospital', '135,New Eskaton Road,Dhaka-1000', 'Tel:8614545-9/9670435', 'N/A', 'N/A', 6),
(54, 'Center for Kidney Diseases & Urology Hospital', 'Road#03,H#32,Shamoli,Dhaka', 'Tel:+88029127306', 'N/A', 'N/A', 7),
(55, 'Rainbow Heart Ltd.', '68,Shatmasjid Road,Dhanmondi,Dhaka-1209', 'Tel:9131207,9115602', 'N/A', 'www.rainbowheartltd.com', 3),
(56, 'Padma General Hospital Ltd.', '290,Bir Uttam CR Datta Road ,Dhaka-1205(New)', 'Tel:9661528,9662502', 'N/A', 'N/A', 3),
(57, 'Shahabuddin Medical College Hospital', 'House # 15,16, Road # 113/A, Gulshan-2, Dhaka-1212', 'Phone: 9862593-4, 9884501, 9863387,Mobile : 01915-', 'smch@shahabuddinmedicai.org', 'www.shahabuddinmedical.&rg', 1),
(58, 'Eden Multi-Care Hospital', '753,Satmasjid Road (beside star kabab),Dhanmondi,Dhaka-1209', 'Mbl:01718-735269,01715-087661', 'N/A', 'N/A', 3),
(59, 'Dipham Hospital & Research Centre (Pvt.) Ltd.', 'House#16, Road # 16 (Old 27), Genetic Plaza, Dhanmondi, Dhaka-1209', 'Phone : 81 53301, 8125225, 8121460,9138399,9115357', 'diphamhospital@yahoo.com', 'N/A', 3),
(60, 'Al-Helal Specialized Hospital Ltd.', '150,Begum Rokeya Sarani,Senpara,Mirpur-10,Dhaka-1216', 'Tel:9006820,9024716,Mob:01818-169907,01678-090793', 'N/A', 'N/A', 8),
(61, 'Dhaka Trauma Center and Specialised Hospital Ltd.', '23/6 Rupayan Shelford(3rd Floor),Block-B,Bir Uttam A.N.M Nuruzzaman Road(Mirpur Road)Shyamoli,Muhammadpur,Dhaka-1207', 'Mobile: 01789231257, 01911775774', 'N/A', 'N/A', 9),
(62, 'Green Life Hospital Ltd.', '32,Green Road,Dhaka-1205', 'Phone#9612345-54', 'N/A', 'N/A', 5),
(63, 'Bangladesh Physiotherapy Hospital', 'Kohinoor Tower-1,Level-3,House#SWE-7,Road#7,Gulshan-1,Dhaka-1212', 'Tel:+88029886704', 'Info@bphospital.org', 'www. bphospital.org', 1),
(64, 'M. General Hospital & Diagnostic Centre', 'House#35,Road#01,Section#10,Mirpur,Dhaka-1216 ', 'Hotline:01971-343038', 'N/A', 'N/A', 8),
(65, 'Care Zone Hospital Ltd.', '24,Garib-E-Newaz Aveniue,Sector-11,Uttara,Dhaka-1230', 'Tel:+88028958440,Mbl:01970-504010,11,12', 'Email:mail@carezonebd.com', 'Web:www.carezonebd.com', 4),
(66, 'ICDDRB,Mohakhali Kacha Bazar', 'Mohakhali,Dhaka', 'Tel: 8806523-32', 'N/A', 'www.icddrb.org', 11),
(67, 'Nostrum Diagnostic Centre & Hospital', 'Alauddin Avenue, House #22, Sector #6, Uttara ', 'Telephone : 02-8954517 ', 'N/A', 'N/A', 4),
(68, 'Delta Medical College Hospital', '26/2 Dar-Us-Salam Rd, Dhaka 1216,', 'Phone:+880 2-8017151 .', 'N/A', 'N/A', 8),
(69, 'Gastro Liver Hospital & Research Institute Ltd.', '77/A, East Rajabarzar, west Panthapath ,Dhaka-1215', 'Tel:9131537', 'Email:gastroliverbd@gmail.com  ', 'N/A', 10);

-- --------------------------------------------------------

--
-- Table structure for table `specialist`
--

CREATE TABLE `specialist` (
  `sp_id` int(11) NOT NULL auto_increment,
  `sp_name` varchar(50) NOT NULL,
  `hosp_id` int(11) NOT NULL,
  PRIMARY KEY  (`sp_id`),
  KEY `hosp_id` (`hosp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `specialist`
--

INSERT INTO `specialist` (`sp_id`, `sp_name`, `hosp_id`) VALUES
(1, 'General Surgeon', 33),
(2, 'Gynae & Obstetrician', 33),
(3, 'Child Specialist', 33),
(4, 'Oral & Dental Care', 33),
(5, 'ENT Specialist & Surgeon', 33),
(6, 'Cardiothorasic Heart Surgeon', 33),
(7, 'ENT specialist', 33),
(8, 'Physiotherapist', 33),
(9, 'Cardiologist', 33),
(10, 'Urologist & Andrologist', 33),
(11, 'Skin & Allergy Specialist', 33),
(12, 'INTERNAL MEDICINE', 34),
(13, 'ICU', 34),
(14, 'CARDIOLOGY (INTERVENTIONAL)', 34),
(15, 'CARDIAC & VASCULAR SURGERY', 34),
(16, 'GASTROENTEROLOGY', 34),
(17, 'GENERAL SURGERY', 34),
(18, 'UROLOGY', 34),
(19, 'DENTAL', 34),
(20, 'ENDOCRINOLOGY', 34),
(21, 'ONCOLOGY', 34),
(22, 'CLINICAL HAEMATOLOGY', 34),
(23, 'PEDIATRICS', 34),
(24, 'NEONATOLOGY', 34),
(25, 'PEDIATRIC SURGERY', 34),
(26, 'DERMATOLOGY', 34),
(27, 'ENT, HEAD & NECK SURGERY', 34),
(28, 'ORTHOPEDICS', 34),
(29, 'NEURO SURGERY', 34),
(30, 'NEUROMEDICIIME', 34),
(31, 'OBGYN', 34),
(32, 'PATHOLOGY & LAB. MEDICINE', 34),
(33, 'RADIOLOGY', 34),
(34, 'OPHTHALMOLOGY', 34),
(35, 'CHEST', 34),
(36, 'NEPHROLOGY', 34),
(37, 'PHYSICAL MEDICINE', 34),
(38, 'PSYCHIATRY', 34),
(39, 'ANESTHESIOLOGY', 34),
(40, 'IVF', 34),
(41, 'EXECUTIVE HEALTH CHECK-UP', 34),
(42, 'EMERGENCY', 34);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
  ADD CONSTRAINT `doctors_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `specialist` (`sp_id`);

--
-- Constraints for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD CONSTRAINT `hospitals_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`);

--
-- Constraints for table `specialist`
--
ALTER TABLE `specialist`
  ADD CONSTRAINT `specialist_ibfk_1` FOREIGN KEY (`hosp_id`) REFERENCES `hospitals` (`hosp_id`);
